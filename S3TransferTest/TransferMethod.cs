﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TransferMethod.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the TransferMethod type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace S3TransferTest
{
    /// <summary>
    /// The transfer method.
    /// </summary>
    public enum TransferMethod
    {
        /// <summary>
        /// The transfer utility upload directory.
        /// </summary>
        TransferUtilityUploadDirectory = 0,

        /// <summary>
        /// The transfer utility upload.
        /// </summary>
        TransferUtilityUpload = 1,

        /// <summary>
        /// The amazon s 3 multipart upload.
        /// </summary>
        AmazonS3MultipartUpload = 2,

        /// <summary>
        /// The amazon s 3 put object.
        /// </summary>
        AmazonS3PutObject = 3,

        /// <summary>
        /// The transfer utility upload single transfer utility.
        /// </summary>
        TransferUtilityUploadSingleTransferUtility = 4
    }
}
