﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace S3TransferTest
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;

    using Amazon.Runtime;
    using Amazon.S3;
    using Amazon.S3.Model;
    using Amazon.S3.Util;

    using Newtonsoft.Json;

    using UrlCombineLib;

    /// <summary>
    /// The program.
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Reviewed. Suppression is OK here.")]
    public class Program
    {
        /// <summary>
        /// The method selector.
        /// </summary>
        private static readonly Dictionary<TransferMethod, Action> Selector =
            new Dictionary<TransferMethod, Action>
                {
                    {
                        TransferMethod.TransferUtilityUploadDirectory,
                        UploadDirectoryTransferUtility
                    },
                    {
                        TransferMethod.TransferUtilityUpload,
                        UploadParallelTransferUtility
                    },
                    {
                        TransferMethod.AmazonS3MultipartUpload,
                        UploadDirectoryMultiPartUpload
                    },
                    {
                        TransferMethod.AmazonS3PutObject,
                        UploadDirectoryPutObject
                    },
                    {
                        TransferMethod.TransferUtilityUploadSingleTransferUtility,
                        UploadParallelSingleTransferUtility
                    }
                };

        /// <summary>
        /// The settings.
        /// </summary>
        private static Settings settigns;

        /// <summary>
        /// The credentials.
        /// </summary>
        private static AWSCredentials credentials;

        /// <summary>
        /// Gets or sets the threads.
        /// </summary>
        public static int Threads { get; set; }

        /// <summary>
        /// Gets or sets the random prefix.
        /// </summary>
        public static string RandomPrefix => settigns.PrefixEverything ? $"{RandomString(4)}{TestSuffix}" : string.Empty;

        /// <summary>
        /// The test suffix.
        /// </summary>
        private static string TestSuffix => "UpTest";

        /// <summary>
        /// The main.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public static void Main(string[] args)
        {
            try
            {
                settigns = ReadSettings();

                if (!string.IsNullOrEmpty(settigns.AwsAccessKeyId))
                {
                    credentials = new BasicAWSCredentials(settigns.AwsAccessKeyId, settigns.AwsSecretKey);
                }

                Threads = settigns.NumberOfThreads;

                ServicePointManager.DefaultConnectionLimit = settigns.SystemConnectionLimit == 0
                                                                 ? settigns.NumberOfThreads
                                                                 : settigns.SystemConnectionLimit;

                settigns.RemotePath = UrlCombine.Combine(settigns.RemotePath, $"{RandomString(8)}{TestSuffix}");
                var resultsFilename = Path.Combine(settigns.ResultsPath, $"Results_{DateTime.UtcNow:yy-MM-dd-HH-mm-ss}.txt");
                Console.WriteLine($"Starting sending files and logging to file {resultsFilename}");


                using (var cc = new ConsoleCopy(resultsFilename))
                {
                    var stopWatch = new Stopwatch();
                    Console.WriteLine($"LocalPath: {settigns.LocalPath}");
                    Console.WriteLine(
                        settigns.PrefixEverything
                            ? $"Using forced 'PrefixEverything', each file will be randomly prefixed"
                            : $"RemotePath: {settigns.RemotePath}");
                    Console.WriteLine($"Method: {settigns.Method}");
                    Console.WriteLine($"Threads: {settigns.NumberOfThreads}");
                    Console.WriteLine($"ServicePointManager.DefaultConnectionLimit: {ServicePointManager.DefaultConnectionLimit}");
                    Console.WriteLine($"Method: {settigns.Method}");
                    Console.WriteLine($"Start Time: {DateTime.UtcNow}");
                    stopWatch.Start();
                    Selector[settigns.Method].Invoke();
                    stopWatch.Stop();
                    Console.WriteLine($"Finish Time: {DateTime.UtcNow}");
                    Console.WriteLine($"Elapsed: {stopWatch.Elapsed.TotalSeconds}s");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception thrown: {e}");
            }
        }

        /// <summary>
        /// The random string.
        /// </summary>
        /// <param name="length">
        /// The length.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string RandomString(int length)
        {
            var random = new Random();
            string chars = settigns.RandomCharactersPool;
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        /// <summary>
        /// The read settings.
        /// </summary>
        /// <returns>
        /// The <see cref="Settings"/>.
        /// </returns>
        private static Settings ReadSettings()
        {
            var jsonContent = File.ReadAllText("settings.json");
            return JsonConvert.DeserializeObject<Settings>(jsonContent);
        }

        /// <summary>
        /// The upload parallel transfer utility.
        /// </summary>
        private static void UploadParallelTransferUtility()
        {
            var s3ExternalStorageDriver = new AmazonS3ExternalStorageDriver(settigns.RemotePath, credentials);
            var dir = new DirectoryInfo(settigns.LocalPath);
            var files = dir.GetFiles("*", SearchOption.AllDirectories);
            Parallel.ForEach(
                files,
                new ParallelOptions { MaxDegreeOfParallelism = settigns.NumberOfThreads },
                currentFile =>
                    {
                        var localUri = new Uri(settigns.LocalPath, UriKind.Absolute);
                        var fullUri = new Uri(currentFile.FullName, UriKind.Absolute);
                        s3ExternalStorageDriver.UploadFile(
                            currentFile.FullName,
                            UrlCombine.Combine(settigns.RemotePath, localUri.MakeRelativeUri(fullUri).ToString()));
                    });
        }

        /// <summary>
        /// The upload parallel single transfer utility.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        private static void UploadParallelSingleTransferUtility()
        {
            var s3ExternalStorageDriver = new AmazonS3ExternalStorageDriver(settigns.RemotePath, credentials);
            var dir = new DirectoryInfo(settigns.LocalPath);
            var files = dir.GetFiles("*", SearchOption.AllDirectories);

            var location = s3ExternalStorageDriver.ParseRemotePath(settigns.RemotePath);
            var transferUtility = s3ExternalStorageDriver.GetTransferUtility(location.Region);

            Parallel.ForEach(
                files,
                new ParallelOptions { MaxDegreeOfParallelism = settigns.NumberOfThreads },
                currentFile =>
                    {
                        var localUri = new Uri(settigns.LocalPath, UriKind.Absolute);
                        var fullUri = new Uri(currentFile.FullName, UriKind.Absolute);
                        s3ExternalStorageDriver.UploadFile(
                            currentFile.FullName,
                            UrlCombine.Combine(settigns.RemotePath, localUri.MakeRelativeUri(fullUri).ToString()),
                            transferUtility);
                    });
        }

        /// <summary>
        /// The upload directory transfer utility.
        /// </summary>
        private static void UploadDirectoryTransferUtility()
        {
            var s3ExternalStorageDriver = new AmazonS3ExternalStorageDriver(settigns.RemotePath, credentials);
            s3ExternalStorageDriver.UploadDirectory(settigns.LocalPath, settigns.RemotePath);
        }

        /// <summary>
        /// The upload directory multi part upload.
        /// </summary>
        private static void UploadDirectoryMultiPartUpload()
        {
            var dir = new DirectoryInfo(settigns.LocalPath);
            var files = dir.GetFiles("*", SearchOption.AllDirectories);
            Parallel.ForEach(
                files,
                new ParallelOptions { MaxDegreeOfParallelism = settigns.NumberOfThreads },
                currentFile =>
                    {
                        var localUri = new Uri(settigns.LocalPath, UriKind.Absolute);
                        var fullUri = new Uri(currentFile.FullName, UriKind.Absolute);
                        UploadFileMultiPartTransfer(currentFile.FullName, UrlCombine.Combine(settigns.RemotePath, localUri.MakeRelativeUri(fullUri).ToString()));
                    });
        }

        /// <summary>
        /// The upload file multi part transfer.
        /// </summary>
        /// <param name="localPath">
        /// The local path.
        /// </param>
        /// <param name="remotePath">
        /// The remote path.
        /// </param>
        private static void UploadFileMultiPartTransfer(string localPath, string remotePath)
        {
            var uri = new AmazonS3Uri(remotePath);
            string existingBucketName = uri.Bucket;
            var keyName = string.IsNullOrEmpty(RandomPrefix) ? uri.Key : UrlCombine.Combine(RandomPrefix, uri.Key);

            string filePath = localPath;
            IAmazonS3 s3Client = credentials != null
                                     ? new AmazonS3Client(credentials, uri.Region)
                                     : new AmazonS3Client(uri.Region);

            // List to store upload part responses.
            List<UploadPartResponse> uploadResponses = new List<UploadPartResponse>();

            // 1. Initialize.
            InitiateMultipartUploadRequest initiateRequest = new InitiateMultipartUploadRequest
            {
                BucketName = existingBucketName,
                Key = keyName
            };

            InitiateMultipartUploadResponse initResponse =
                s3Client.InitiateMultipartUpload(initiateRequest);

            // 2. Upload Parts.
            long contentLength = new FileInfo(filePath).Length;
            // long partSize = 5 * (long)Math.Pow(2, 20); // 5 MB

            try
            {
                long filePosition = 0;
                UploadPartRequest uploadRequest = new UploadPartRequest
                {
                    BucketName = existingBucketName,
                    Key = keyName,
                    UploadId = initResponse.UploadId,
                    PartNumber = 1,
                    PartSize = contentLength,
                    FilePosition = filePosition,
                    FilePath = filePath
                };

                // Upload part and add response to our list.
                uploadResponses.Add(s3Client.UploadPart(uploadRequest));

                // Step 3: complete.
                CompleteMultipartUploadRequest completeRequest = new CompleteMultipartUploadRequest
                {
                    BucketName = existingBucketName,
                    Key = keyName,
                    UploadId = initResponse.UploadId,
                    //PartETags = new List<PartETag>(uploadResponses)
                };
                completeRequest.AddPartETags(uploadResponses);

                CompleteMultipartUploadResponse completeUploadResponse =
                    s3Client.CompleteMultipartUpload(completeRequest);
            }
            catch (Exception exception)
            {
                Console.WriteLine("Exception occurred: {0}", exception.Message);
                AbortMultipartUploadRequest abortMPURequest = new AbortMultipartUploadRequest
                {
                    BucketName = existingBucketName,
                    Key = keyName,
                    UploadId = initResponse.UploadId
                };
                s3Client.AbortMultipartUpload(abortMPURequest);
            }
        }

        /// <summary>
        /// The upload directory multi part upload.
        /// </summary>
        private static void UploadDirectoryPutObject()
        {
            var dir = new DirectoryInfo(settigns.LocalPath);
            var files = dir.GetFiles("*", SearchOption.AllDirectories);
            Parallel.ForEach(
                files,
                new ParallelOptions { MaxDegreeOfParallelism = settigns.NumberOfThreads },
                currentFile =>
                    {
                        var localUri = new Uri(settigns.LocalPath, UriKind.Absolute);
                        var fullUri = new Uri(currentFile.FullName, UriKind.Absolute);
                        UploadFilePutObject(currentFile.FullName, UrlCombine.Combine(settigns.RemotePath, localUri.MakeRelativeUri(fullUri).ToString()));
                    });
        }

        /// <summary>
        /// The upload file put object.
        /// </summary>
        /// <param name="localPath">
        /// The local path.
        /// </param>
        /// <param name="remotePath">
        /// The remote path.
        /// </param>
        private static void UploadFilePutObject(string localPath, string remotePath)
        {
            var uri = new AmazonS3Uri(remotePath);
            string existingBucketName = uri.Bucket;
            var keyName = string.IsNullOrEmpty(RandomPrefix) ? uri.Key : UrlCombine.Combine(RandomPrefix, uri.Key);
            string filePath = localPath;
            IAmazonS3 s3Client = credentials != null
                                     ? new AmazonS3Client(credentials, uri.Region)
                                     : new AmazonS3Client(uri.Region);
            PutObjectRequest request = new PutObjectRequest()
            {
                BucketName = existingBucketName,
                Key = keyName,
                FilePath = filePath
            };
            PutObjectResponse response = s3Client.PutObject(request);
        }
    }
}
