﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AwsObjectLocation.cs" company="ProofHQ Limited">
//   Copyright (c)
// </copyright>
// <summary>
//   Defines the AwsObjectLocation type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace S3TransferTest
{
    using Amazon;

    /// <summary>
    /// The aws object location.
    /// </summary>
    public class AwsObjectLocation
    {
        /// <summary>
        /// Gets or sets the bucket.
        /// </summary>
        public string Bucket { get; set; }

        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the region.
        /// </summary>
        public RegionEndpoint Region { get; set; }
    }
}
