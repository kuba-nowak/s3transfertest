// -------------------------------------------------------------------public -------------------------------------------------
// <copyright file="AmazonS3ExternalStorageDriver.cs" company="ProofHQ Limited">
//   Copyright (c)
// </copyright>
// <summary>
//   Defines the AmazonS3ExternalStorageDriver type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace S3TransferTest
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    using Amazon;
    using Amazon.Runtime;
    using Amazon.S3;
    using Amazon.S3.Model;
    using Amazon.S3.Transfer;
    using Amazon.S3.Util;

    using UrlCombineLib;

    /// <summary>
    /// The amazon s 3 external storage driver.
    /// </summary>
    public class AmazonS3ExternalStorageDriver : BaseExternalStorageDriver
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AmazonS3ExternalStorageDriver"/> class.
        /// </summary>
        /// <param name="path">
        ///     The path.
        /// </param>
        /// <param name="credentials">
        /// The credentials.
        /// </param>
        public AmazonS3ExternalStorageDriver(string path, AWSCredentials credentials = null)
        {
            var location = this.ParseRemotePath(path);
            this.Bucket = location.Bucket;
            var prefix = location.Key ?? string.Empty;
            this.Prefix = prefix.EndsWith("/") ? prefix : string.Empty;

            this.AwsRegion = location.Region;
            this.Credentials = credentials;
        }

        /// <summary>
        /// Gets the bucket.
        /// </summary>
        public string Bucket { get; }

        /// <summary>
        /// Gets the prefix.
        /// </summary>
        public string Prefix { get; }

        /// <summary>
        /// Gets the aws region.
        /// </summary>
        public RegionEndpoint AwsRegion { get; }

        /// <summary>
        /// Gets the aws credentials.
        /// </summary>
        protected AWSCredentials Credentials { get; }

        /// <summary>
        /// The is s3 endpoint.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool IsAmazonS3Endpoint(string path)
        {
            try
            {
                return AmazonS3Uri.IsAmazonS3Endpoint(path);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The file exists.
        /// </summary>
        /// <param name="remotePath">
        /// The remote path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool FileExists(string remotePath)
        {
            var location = this.ParseRemotePath(remotePath);

            try
            {
                using (var client = this.GetClient(location.Region))
                {
                    client.GetObjectMetadata(
                        new GetObjectMetadataRequest { BucketName = location.Bucket, Key = location.Key });
                }

                return true;
            }
            catch (AmazonS3Exception ex)
            {
                if (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    return false;
                }

                // status wasn't not found, so throw the exception
                throw;
            }
        }

        /// <summary>
        /// The download file.
        /// </summary>
        /// <param name="remotePath">
        /// The remote path.
        /// </param>
        /// <param name="localPath">
        /// The local path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool DownloadFile(string remotePath, string localPath)
        {
            var location = this.ParseRemotePath(remotePath);

            using (var client = this.GetClient(location.Region))
            {
                var request = new GetObjectRequest
                {
                    BucketName = location.Bucket,
                    Key = location.Key
                };

                using (var response = client.GetObject(request))
                {
                    response.WriteResponseStreamToFile(localPath);
                }
            }

            return true;
        }

        /// <summary>
        /// Download directory from remote bucket. 
        /// </summary>
        /// <remarks>
        /// Please note this downloads all files in flat structure to specified folder.
        /// </remarks>
        /// <param name="remotePath">
        /// The remote path.
        /// </param>
        /// <param name="localPath">
        /// The local path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool DownloadDirectory(string remotePath, string localPath)
        {
            // TODO Rewrite this funcionality if reused, so that it recreates the file structure in target local locationl.
            // At the time being this downoads all files in target location to flat directory
            using (var client = this.GetClient(this.AwsRegion))
            {
                var location = this.ParseRemotePath(remotePath);
                var keys = client.GetAllObjectKeys(location.Bucket, location.Key, null);

                Parallel.ForEach(
                    keys,
                    new ParallelOptions { MaxDegreeOfParallelism = 10 },
                    key =>
                        {
                            if (string.IsNullOrWhiteSpace(key))
                            {
                                throw new Exception($"{location.Key} {location.Bucket}");
                            }

                            using (var fileClient = this.GetClient(this.AwsRegion))
                            using (var response = fileClient.GetObject(this.Bucket, key))
                            {
                                response.WriteResponseStreamToFile(Path.Combine(localPath, Path.GetFileName(key)));
                            }
                        });
            }

            return true;
        }

        /// <summary>
        /// The upload file.
        /// </summary>
        /// <param name="localPath">
        /// The local path.
        /// </param>
        /// <param name="remotePath">
        /// The remote path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool UploadFile(string localPath, string remotePath)
        {
            if (IsAmazonS3Endpoint(localPath))
            {
                return this.CopyObject(localPath, remotePath);
            }

            var location = this.ParseRemotePath(remotePath);

            using (var transferUtility = this.GetTransferUtility(location.Region))
            {
                transferUtility.Upload(localPath, location.Bucket, location.Key);
            }

            return true;
        }

        /// <summary>
        /// The upload file.
        /// </summary>
        /// <param name="localPath">
        /// The local path.
        /// </param>
        /// <param name="remotePath">
        /// The remote path.
        /// </param>
        /// <param name="transferUtility">
        /// The transfer utility.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool UploadFile(string localPath, string remotePath, ITransferUtility transferUtility)
        {
            if (IsAmazonS3Endpoint(localPath))
            {
                return this.CopyObject(localPath, remotePath);
            }

            var location = this.ParseRemotePath(remotePath);
            transferUtility.Upload(localPath, location.Bucket, location.Key);

            return true;
        }

        /// <summary>
        /// The upload directory.
        /// </summary>
        /// <param name="localDirectory">
        /// The local directory.
        /// </param>
        /// <param name="remoteDirectory">
        /// The remote directory.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool UploadDirectory(string localDirectory, string remoteDirectory)
        {
            var location = this.ParseRemotePath(remoteDirectory);

            using (var transferUtility = this.GetTransferUtility(location.Region))
            {
                var request = new TransferUtilityUploadDirectoryRequest
                {
                    BucketName = location.Bucket,
                    Directory = localDirectory,
                    KeyPrefix = location.Key,
                    SearchOption = SearchOption.AllDirectories
                };

                transferUtility.UploadDirectory(request);
            }

            return true;
        }

        /// <summary>
        /// The delete directory.
        /// </summary>
        /// <param name="directory">
        /// The directory.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool DeleteDirectory(string directory)
        {
            var objectsToDelete = this.GetObjectsKeysInFolder(directory).ToArray();
            if (objectsToDelete.Any())
            {
                var location = this.ParseRemotePath(directory);

                using (var client = this.GetClient(location.Region))
                {
                    var deleteObjectRequest = new DeleteObjectsRequest { BucketName = location.Bucket };
                    foreach (var key in objectsToDelete)
                    {
                        deleteObjectRequest.AddKey(key);
                    }

                    client.DeleteObjects(deleteObjectRequest);
                }
            }

            return true;
        }

        /// <summary>
        /// The generate name.
        /// </summary>
        /// <param name="objectName">
        /// The object name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string GenerateName(string objectName)
        {
            if (IsAmazonS3Endpoint(objectName))
            {
                return objectName;
            }

            var objectLocation = this.ParseRemotePath(objectName);

            return string.Format("https://s3-{0}.amazonaws.com/{1}/{2}", this.AwsRegion.SystemName, this.Bucket, objectLocation.Key);
        }

        /// <summary>
        /// The delete file.
        /// </summary>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool DeleteFile(string fileName)
        {
            var location = this.ParseRemotePath(fileName);

            using (var client = this.GetClient(location.Region))
            {
                var request = new DeleteObjectRequest() { BucketName = this.Bucket, Key = location.Key };
                client.DeleteObject(request);
            }

            return true;
        }

        /// <summary>
        /// The get objects keys in folder.
        /// </summary>
        /// <param name="directory">
        /// The directory.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{String}"/>.
        /// </returns>
        private IEnumerable<string> GetObjectsKeysInFolder(string directory)
        {
            var location = this.ParseRemotePath(directory);

            using (var client = this.GetClient(location.Region))
            {
                var prefix = location.Key;
                if (!prefix.EndsWith("/"))
                {
                    prefix += "/";
                }

                var request = new ListObjectsRequest { BucketName = location.Bucket, Prefix = prefix };

                return client.ListObjects(request).S3Objects.Select(o => o.Key);
            }
        }

        /// <summary>
        /// The get client.
        /// </summary>
        /// <param name="region">
        /// The region.
        /// </param>
        /// <returns>
        /// The <see cref="IAmazonS3"/>.
        /// </returns>
        private IAmazonS3 GetClient(RegionEndpoint region)
        {
            var config = new AmazonS3Config
            {
                ConnectionLimit = Program.Threads,
                RegionEndpoint = region
            };
            return this.Credentials != null ? new AmazonS3Client(this.Credentials, config) : new AmazonS3Client(config);
        }

        /// <summary>
        /// The get transfer utility.
        /// </summary>
        /// <param name="region">The region.</param>
        /// <returns>
        /// The <see cref="ITransferUtility"/>.
        /// </returns>
        public ITransferUtility GetTransferUtility(RegionEndpoint region)
        {
            var config = new TransferUtilityConfig { ConcurrentServiceRequests = Program.Threads };
            return new TransferUtility(this.GetClient(region), config);
        }

        /// <summary>
        /// The parse remote path.
        /// </summary>
        /// <param name="remotePath">
        /// The remote path.
        /// </param>
        /// <returns>
        /// The <see cref="AwsObjectLocation"/>.
        /// </returns>
        public AwsObjectLocation ParseRemotePath(string remotePath)
        {
            if (IsAmazonS3Endpoint(remotePath))
            {
                var uri = new AmazonS3Uri(remotePath);
                if (!string.IsNullOrEmpty(this.Bucket) && uri.Bucket != this.Bucket)
                {
                    throw new InvalidOperationException(
                        $"The bucket in path ${remotePath} does not match the bucket assigned to current driver: ${this.Bucket}");
                }

                return new AwsObjectLocation
                {
                    Bucket = uri.Bucket,
                    Region = uri.Region,
                    Key = string.IsNullOrEmpty(Program.RandomPrefix)
                                                    ? uri.Key
                                                    : UrlCombine.Combine(Program.RandomPrefix, uri.Key)
                };
            }

            return new AwsObjectLocation { Bucket = this.Bucket, Key = this.GenerateKey(remotePath), Region = this.AwsRegion };
        }

        /// <summary>
        /// The generate key.
        /// </summary>
        /// <param name="locationKey">
        /// The location key.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GenerateKey(string locationKey)
        {
            return string.Concat(this.Prefix, locationKey.Replace("\\", "/").TrimStart('/'));
        }

        /// <summary>
        /// The copy object.
        /// </summary>
        /// <param name="sourcePath">
        /// The source path.
        /// </param>
        /// <param name="remotePath">
        /// The remote path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool CopyObject(string sourcePath, string remotePath)
        {
            var location = this.ParseRemotePath(remotePath);
            var source = new AmazonS3Uri(sourcePath);

            CopyObjectRequest request = new CopyObjectRequest()
            {
                SourceBucket = source.Bucket,
                SourceKey = source.Key,
                DestinationBucket = location.Bucket,
                DestinationKey = location.Key
            };

            using (var client = this.GetClient(location.Region))
            {
                client.CopyObject(request);
            }

            return true;
        }
    }
}
