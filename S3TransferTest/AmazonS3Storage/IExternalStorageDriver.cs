﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IExternalStorageDriver.cs" company="ProofHQ Limited">
//   Copyright (c)
// </copyright>
// <summary>
//   Defines the IExternalStorageDriver type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace S3TransferTest
{
    /// <summary>
    /// The StorageDriver interface.
    /// </summary>
    public interface IExternalStorageDriver
    {
        /// <summary>
        /// The upload file.
        /// </summary>
        /// <param name="localPath">
        /// The local path.
        /// </param>
        /// <param name="remotePath">
        /// The remote path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool UploadFile(string localPath, string remotePath);

        /// <summary>
        /// The download file.
        /// </summary>
        /// <param name="remotePath">
        /// The remote path.
        /// </param>
        /// <param name="localPath">
        /// The local path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool DownloadFile(string remotePath, string localPath);

        /// <summary>
        /// The download directory.
        /// </summary>
        /// <param name="remotePath">
        /// The remote path.
        /// </param>
        /// <param name="localPath">
        /// The local path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool DownloadDirectory(string remotePath, string localPath);

        /// <summary>
        /// The file exists.
        /// </summary>
        /// <param name="remotePath">
        /// The remote path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool FileExists(string remotePath);

        /// <summary>
        /// The upload directory.
        /// </summary>
        /// <param name="localDirectory">
        /// The local directory.
        /// </param>
        /// <param name="remoteDirectory">
        /// The remote directory.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool UploadDirectory(string localDirectory, string remoteDirectory);

        /// <summary>
        /// The generate name.
        /// </summary>
        /// <param name="objectName">
        /// The object name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GenerateName(string objectName);

        /// <summary>
        /// The delete directory.
        /// </summary>
        /// <param name="directory">
        /// The directory.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool DeleteDirectory(string directory);

        /// <summary>
        /// The delete file.
        /// </summary>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool DeleteFile(string fileName);
    }
}
