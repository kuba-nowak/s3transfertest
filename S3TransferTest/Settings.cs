﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Settings.cs" company="">
//   
// </copyright>
// <summary>
//   The settings.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace S3TransferTest
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The settings.
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// Gets or sets the AWS access key id.
        /// </summary>
        public string AwsAccessKeyId { get; set; }

        /// <summary>
        /// Gets or sets the AWS secret key.
        /// </summary>
        public string AwsSecretKey { get; set; }

        /// <summary>
        /// Gets or sets the number of threads.
        /// </summary>
        public int NumberOfThreads { get; set; }

        /// <summary>
        /// Gets or sets the system connection limit.
        /// </summary>
        public int SystemConnectionLimit { get; set; }

        /// <summary>
        /// Gets or sets the local path.
        /// </summary>
        public string LocalPath { get; set; }

        /// <summary>
        /// Gets or sets the remote path.
        /// </summary>
        public string RemotePath { get; set; }

        /// <summary>
        /// Gets or sets the results path.
        /// </summary>
        public string ResultsPath { get; set; }

        /// <summary>
        /// Gets or sets the method.
        /// </summary>
        public TransferMethod Method { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether prefix everything.
        /// </summary>
        public bool PrefixEverything { get; set; }

        /// <summary>
        /// Gets or sets the random characters pool.
        /// </summary>
        public string RandomCharactersPool { get; set; }
    }
}
